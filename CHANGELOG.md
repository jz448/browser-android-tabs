# Changelog

## [1.0.98](https://github.com/brave/browser-android-tabs/releases/tag/v1.0.98-rc.2)

- Upgraded Chromium to 75.0.3770.89 ([#1681](https://github.com/brave/browser-android-tabs/issues/1681))
- Fixed sync related issues 
  - New bookmarks not being sync'd ([#1702](https://github.com/brave/browser-android-tabs/issues/1702))
  - Android device getting removed from sync chain ([#1354](https://github.com/brave/browser-android-tabs/issues/1354))
  - Frequent disconnect from sync chain ([#1535](https://github.com/brave/browser-android-tabs/issues/1535))